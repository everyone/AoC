const solution = (input: string, topThree?: true): string => {
  let mostCalories: number[] = [];
  for (const listOfCalories of input.split("\n\n")) {
    let totalCalories = 0;
    for (const calory of listOfCalories.split("\n")) {
      totalCalories += Number(calory);
    }
    mostCalories.push(totalCalories);
  }

  mostCalories = mostCalories.toSorted((a, b) => b - a);
  return !topThree ? String(mostCalories[0]) : String(
    mostCalories.splice(0, 3).reduce((p, c) => p + c),
  );
};

import { assertEquals } from "https://deno.land/std@0.204.0/assert/mod.ts";

Deno.test("example input", async (context) => {
  const example_input = [
    "1000",
    "2000",
    "3000",
    "",
    "4000",
    "",
    "5000",
    "6000",
    "",
    "7000",
    "8000",
    "9000",
    "",
    "10000",
  ].join("\n");

  await context.step(
    "part 1",
    () => assertEquals(solution(example_input), "24000"),
  );
  await context.step(
    "part 2",
    () => assertEquals(solution(example_input, true), "45000"),
  );
});

Deno.test("input", async (context) => {
  const input = await Deno.readTextFile("2022/01/input");
  await context.step(
    "part 1",
    () => assertEquals(solution(input), "72511"),
  );
  await context.step(
    "part 2",
    () => assertEquals(solution(input, true), "212117"),
  );
});
