const solution = (input: string, isPart2?: true): number => {
  let gameScore = 0;
  enum outcome {
    Lost = 0,
    Draw = 3,
    Won = 6,
  }
  enum shapes {
    Rock = 1,
    Paper = 2,
    Scissor = 3,
  }
  const game = !isPart2
    ? {
      "A X": shapes.Rock + outcome.Draw,
      "B X": shapes.Rock + outcome.Lost,
      "C X": shapes.Rock + outcome.Won,
      "A Y": shapes.Paper + outcome.Won,
      "B Y": shapes.Paper + outcome.Draw,
      "C Y": shapes.Paper + outcome.Lost,
      "A Z": shapes.Scissor + outcome.Lost,
      "B Z": shapes.Scissor + outcome.Won,
      "C Z": shapes.Scissor + outcome.Draw,
    }
    : {
      "A X": shapes.Scissor + outcome.Lost,
      "A Y": shapes.Rock + outcome.Draw,
      "A Z": shapes.Paper + outcome.Won,
      "B X": shapes.Rock + outcome.Lost,
      "B Y": shapes.Paper + outcome.Draw,
      "B Z": shapes.Scissor + outcome.Won,
      "C X": shapes.Paper + outcome.Lost,
      "C Y": shapes.Scissor + outcome.Draw,
      "C Z": shapes.Rock + outcome.Won,
    };

  input.split("\n").forEach(
    (value) => gameScore += game[value as keyof typeof game],
  );
  return gameScore;
};

import { assertEquals } from "https://deno.land/std@0.204.0/assert/mod.ts";

Deno.test("example input", async (context) => {
  const example_input = ["A Y", "B X", "C Z"].join("\n");

  await context.step(
    "part 1",
    () => assertEquals(solution(example_input), 15),
  );
  await context.step(
    "part 2",
    () => assertEquals(solution(example_input, true), 12),
  );
});

Deno.test("input", async (context) => {
  const input = await Deno.readTextFile("2022/02/input");
  await context.step(
    "part 1",
    () => assertEquals(solution(input), 14375),
  );
  await context.step(
    "part 2",
    () => assertEquals(solution(input, true), 10274),
  );
});
